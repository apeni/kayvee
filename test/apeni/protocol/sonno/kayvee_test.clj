(ns apeni.protocol.sonno.kayvee-test
  (:require [clojure.test :refer :all]
            [clojure.test.check.generators :as gen]
            [com.gfredericks.test.chuck.generators :as gen']
            [apeni.protocol.sonno.kayvee.fs :refer :all]
            [clojure.test.check.clojure-test :refer [defspec]]
            [testit.core :refer :all]
            [com.gfredericks.test.chuck.clojure-test :refer [for-all checking]]))

(defspec round-trip-spec 100
  (for-all [m (gen/such-that not-empty (gen/map gen/keyword-ns gen/string))
            file-name (gen/such-that not-empty gen/string-alphanumeric)
            :let [result (write-file (str "/Users/arun/.data/" file-name) m)]]
           (facts
            (deref result) =eventually=> m)))
