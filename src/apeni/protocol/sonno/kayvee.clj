(ns apeni.protocol.sonno.kayvee
  (:refer-clojure :exclude [exists? get-in assoc-in update-in dissoc])
  (:require [clojure.data.fressian :as fress]
            [manifold.deferred :as d]
            [manifold.stream :as st]
            [taoensso.nippy :as nippy]
            [clojure.spec.alpha :as s]))

(defprotocol SerDe
  :extend-via-metadata true
  (-content-type [this data])
  (-serialize [this data])
  (-deserialize [this bytes])
  (-hex [this data]))

(defprotocol KayVee
  :extend-via-metadata true
  (-exists? [this k])
  (-get-in [this key-vec])
  (-assoc-in [this key-vec v])
  (-update-in [this key-vec f])
  (-dissoc [this k]))

(def exists? -exists?)
(def get-in -get-in)
(def update-in -update-in)
(def assoc-in -assoc-in)
(def dissoc -dissoc)

(defn get [m k] (get-in m [k]))
(defn update [m k] (update-in m [k]))
(defn assoc [m k] (assoc-in m [k]))
