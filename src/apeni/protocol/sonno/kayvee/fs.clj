(ns apeni.protocol.sonno.kayvee.fs
  (:require [clojure.spec.alpha :as s]
            [manifold.deferred :as d :refer [let-flow]]
            [camel-snake-kebab.core :as csk]
            [clojure.data.fressian :as fress])
  (:import java.nio.ByteBuffer
           java.nio.channels.AsynchronousFileChannel
           [java.nio.file FileSystems StandardOpenOption Paths]))

(defn- fs-default []
  (FileSystems/getDefault))

(defn fs-path [fs & strs]
  (let [[fst & rst] strs]
    (.getPath fs fst (into-array String rst))))

(defn standard-open-option [k]
  (->> (csk/->SCREAMING_SNAKE_CASE_STRING k)
       (StandardOpenOption/valueOf)))

(defn standard-open-options [ks]
  (->> (mapv standard-open-option ks)
       (into-array StandardOpenOption)))

(defn async-file-channel [fp opts]
  (AsynchronousFileChannel/open
   (-> (fs-default) (fs-path fp))
   (standard-open-options opts)))

(defn alloc-read-buffer [ac]
  (ByteBuffer/allocate (.size ac)))

(defn read-file
  [fp]
  (let-flow [ac (async-file-channel fp [:read])
             data-buffer (ByteBuffer/allocate (.size ac))
             lines-read (.read ac data-buffer 0)
             data (fress/read data-buffer)]
    data))

(defn read-and-destroy
  [fp]
  (let-flow [ac (async-file-channel fp [:read :delete-on-close])
             data-buffer (ByteBuffer/allocate (.size ac))
             lines-read (.read ac data-buffer 0)
             data (fress/read data-buffer)]
    data))

(defn write-file [fp val]
  (let-flow [ac (async-file-channel fp [:create :write])
             result (.write ac (fress/write val) 0)]
    val))

(defn update-file [fp f]
  (let-flow [data (read-file fp)]
    (write-file fp (f data))))
